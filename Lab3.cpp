#pragma once
#include "incl.h"
#include "Helper.cpp"

namespace Lab3
{

static int factorial(int i) //Рекурсивная функция для вычисления факториала
{
  if (i==0) return 1;
  else return i*factorial(i-1);
}

    static void TaskOne()
    {
        int n;
        cout << "Enter n:";
        cin >> n;

        double result = sqrt((3 * (n-1)) + sqrt(3 * n)); // Начальное выражение
        while (n > 3)
        {
           n = n-3;
           result = sqrt (n + result);

        }

        cout << result;


    }


static void TaskTwo()
{
    //Этот код настолько не оптимизирован, что на него страшно смотреть, лучше не смотрите ))
    // Ну, он работает же, а про скорость работы ничего сказано не было

    long double summ;
    for (int  i = 1; fabs((pow(-2,i) / factorial(i))) > summ; i++)
    {
        summ = summ + pow(-2,i) / factorial(i);

    }
    cout << "The result is: " << summ;

}

static int TaskSelection() //Функция которая управляет выбором конкетного задания
 {
int numb;
cout << "Please, enter the number of task (or 0 for exit): ";
cin >> numb;

switch (numb)
{
case 0:
    return 0;
    break;
case 1:
    _Helper::GetArt(1);
    TaskOne();
    break;
case 2:
    _Helper::GetArt(2);
    TaskTwo();
    break;
default:
    cout << "Incorrect task number,pls try again;";
}
 }

}
