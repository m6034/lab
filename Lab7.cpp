#include "incl.h"
#include "Helper.cpp"

namespace Lab7
{

static void TaskOne()
{
string path;
cout << "Enter path to file: ";

cin >> path;

ifstream in(path);

int numb;
int summ = 1;

while (in >> numb)
{ summ = summ * numb; }

cout << "Result is: " << pow(summ,2);
}

static void TaskTwo()
{

string path_f;
cout << "Enter path to file f: ";
cin >> path_f;
string path_g;
cout << "Enter path to save file g: ";
cin >> path_g;

ifstream in(path_f);
ofstream of(path_g);

int numb;
while (in >> numb)
{
 if (numb % 3 == 0 && numb % 7 != 0) {of << numb << " ";}
}
//на всякий случай
in.close();
of.close();

cout << "Complete, pls, check file )";
}


static int TaskSelection() //Функция которая управляет выбором конкретного задания
 {
int numb;
cout << "Please, enter the number of task (or 0 for exit): ";
cin >> numb;

switch (numb)
{
case 0:
    return 0;
    break;
case 1:
    _Helper::GetArt(1);
    TaskOne();
    break;
case 2:
    _Helper::GetArt(2);
    TaskTwo();
    break;
default:
    cout << "Incorrect task number,pls try again;";
}
 }

}
