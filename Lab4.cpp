#pragma once
#include "incl.h"
#include "Helper.cpp"

using namespace std;


namespace Lab4
{
    static void TaskOne()
    {
      int bz = 0;
      int n;
      cout << "Enter n:";
      cin >> n;

      double a[n][n];

      for (int i = 0; i < n; i++) //Столбцы
      {

       for (int j = 0; j < n; j++) //Строки
      {

          a[i][j] = cos(pow(i,2) + n);
          if (a[i][j] > 0) {bz++;} //Считаем кол-во положительных чисел

      }
      }

      cout << "Positive numbers: " << bz;

/*
      //Вывод на экран

for(int i = 0; i < n; ++i) {  // Выводим на экран строку i
    for(int j = 0; j < n; ++j)
        cout << a[i][j] << " ";
    cout << endl; // Строка завершается символом перехода на новую строку
}

*/

    }


static void TaskTwo()
{
   string str;
   cout << "Enter string: ";
   cin >> str;

   vector<char> st_array(str.begin(), str.end()); //Инициализируем массив и разбиваем строку на символы

   char last; //Предыдущий символ

   for(vector<int>::size_type i=0;i!=st_array.size();i++) //Перебираем массив
    {
        if (st_array[i] == '!') {cout << "Result is: " << "false"; return;} //Дальше можно даже не перебирать
        else if ((last == 'o') & (st_array[i] == 'n')) {cout << "Result is: " << "true"; return;}
        else if ((last == 'n') & (st_array[i] == 'o')) {cout << "Result is: " << "true"; return;}

        last = st_array[i];

    }

    cout << "Result is: " << "false\r\n";
    return;

}

static int TaskSelection() //Функция которая управляет выбором конкетного задания
 {
int numb;
cout << "Please, enter the number of task (or 0 for exit): ";
cin >> numb;

switch (numb)
{
case 0:
    return 0;
    break;
case 1:
    _Helper::GetArt(1);
    TaskOne();
    break;
case 2:
    _Helper::GetArt(2);
    TaskTwo();
    break;
default:
    cout << "Incorrect task number,pls try again;";
}
 }
}


