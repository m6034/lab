#pragma once
#include "incl.h"
#include "Helper.cpp"

namespace Lab1
{
 static void TaskOne()
{
    double x,y,z;
    cout << "Enter x,y,z:";
    cin >> x >> y >> z;

    double a = y + (x / (pow(y,2) + fabs(sqrt(x)/ (y + pow(x,3) / 3))));
    double b = 1 + (pow(tan(z/2),2));

    cout << "The result a is: " << a << endl;
    cout << "The result b is: " << b << endl;
}

static void TaskTwo()
{
    double a,b,c,d,g,f;
    cout << "Pls, enter a:";
    cin >> a;

    b = a * a; //2 степень
    c = b * a; //3 степень
    d = b * c; // 5 степень
    g = d * d; // 10 степень
    f = g * c; // 13 степень

    cout << "a to the fifth power :" << d << endl;
    cout << "a to the thirteenth power :" << f << endl;
}

static void  TaskThree()
{
    double hypotenuse;
    double first_leg;
    double second_leg;

    cout << "Pls, enter hypotenuse and leg (h l): " << endl;
    cin >> hypotenuse >> first_leg;

    second_leg = sqrt(pow(hypotenuse,2) - pow(first_leg,2));
    cout << "The second leg is: " << second_leg << endl;
}


static int TaskSelection() //Функция которая управляет выбором конкетного задания
 {
int numb;
cout << "Please, enter the number of task (or 0 for exit): ";
cin >> numb;

switch (numb)
{
case 0:
    return 0;
    break;
case 1:
    _Helper::GetArt(1);
    TaskOne();
    break;
case 2:
    _Helper::GetArt(2);
    TaskTwo();
    break;
case 3:
    _Helper::GetArt(3);
    TaskThree();
    break;
default:
    cout << "Incorrect task number,pls try again;";
}
 }
};
