#include "incl.h"
#include "Helper.cpp"

namespace Lab6
{

static void Show_Result(long int StartDayNumber, long int EndDayNumber)
{

  if (StartDayNumber > EndDayNumber)
  {
  cout << "Date Error!";
  }
  else if (StartDayNumber == EndDayNumber)
  {
    cout << "Result: 0 days";
  }
  else
  {
   cout << "Result: " << EndDayNumber - StartDayNumber << " days";
  }


}
static long int GetDayNumber(int day,int month,int year)
{

  long int day_numb = 0;

  for (int i = 0; i < year; i++) //Года
  {
  if (year % 4 == 0) {day_numb = day_numb + 366;} //високосный год
  else {day_numb = day_numb + 365;}
  }

  for (int i = 0; i < month; i++) //Месяца
  {

    switch(i)
    {
    case 1:
    day_numb = day_numb + 31;
    break;
    case 2:
    if (year % 4 == 0 ) {day_numb = day_numb + 29;} //високосный год
    else {day_numb = day_numb + 28;}
    break;
    case 3:
    day_numb = day_numb + 31;
    break;
    case 4:
    day_numb = day_numb + 30;
    break;
    case 5:
    day_numb = day_numb + 31;
    break;
    case 6:
    day_numb = day_numb + 30;
    break;
    case 7:
    day_numb = day_numb + 31;
    break;
    case 8:
    day_numb = day_numb + 31;
    break;
    case 9:
    day_numb = day_numb + 30;
    break;
    case 10:
    day_numb = day_numb + 31;
    break;
    case 11:
    day_numb = day_numb + 30;
    break;
    case 12:
    day_numb = day_numb + 31;
    break;
    }

  }

  day_numb = day_numb + day; //Дни
  return day_numb;
}

static void TaskOne()
{
  int start_day,start_month,start_year;
  int end_day,end_month,end_year;

  cout << "Enter Start (Day Month Year):";
  cin >> start_day >> start_month >> start_year;

  cout << "Enter End (Day Month Year):";
  cin >> end_day >> end_month >> end_year;

  long int StartDayNumber = GetDayNumber(start_day,start_month,start_year);
  long int EndDayNumber = GetDayNumber(end_day,end_month,end_year);

  Show_Result(StartDayNumber,EndDayNumber); //хз зачем, но в задании сказано выделить эту функцию...
}

static int TaskSelection() //Функция которая управляет выбором конкретного задания
 {
int numb;
cout << "Please, enter the number of task (or 0 for exit): ";
cin >> numb;

switch (numb)
{
case 0:
    return 0;
    break;
case 1:
    _Helper::GetArt(1);
    TaskOne();
    break;
default:
    cout << "Incorrect task number,pls try again;";
}
 }
}
