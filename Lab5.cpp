#pragma once
#include "incl.h"
#include "Helper.cpp"

using namespace std;
namespace Lab5
{

static void TaskOne()
{

    double a,b,c;
    cout << "Enter a b c: ";
    cin >> a >> b >>c;

    double result = (max(a, a + b ) + max (a, b + c)) /
    (1 + max(a + b * c, 1.15));

    cout << "Result is: " << result;
}

static void TaskTwo()
{

string first;
string second;
string result;

cout << "Enter first string:";
cin >> first;

cout << "Enter second string:";
cin >> second;

vector<char> first_array(first.begin(),first.end());
vector<char> second_array(second.begin(),second.end());

cout << "Result is: \n";
for (vector<char>::size_type i=0; i <= first_array.size(); i++)
{

bool write = true;
for (vector<char>::size_type b=0; b <= second_array.size(); b++)
{
  if (first_array[i] == first_array[b]) {write = false;}
}

 if (write) {cout << first_array[i];} //Если такого символа не было во второй строке, выводим его в консоль

}}

static int TaskSelection() //Функция которая управляет выбором конкетного задания
 {
int numb;
cout << "Please, enter the number of task (or 0 for exit): ";
cin >> numb;

switch (numb)
{
case 0:
    return 0;
    break;
case 1:
    _Helper::GetArt(1);
    TaskOne();
    break;
case 2:
    _Helper::GetArt(2);
    TaskTwo();
    break;
default:
    cout << "Incorrect task number,pls try again;";
}
 }

}
