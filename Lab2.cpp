#pragma once
#include "incl.h"
#include "Helper.cpp"


namespace Lab2
{
static void TaskOne()
{
 long double x,y,z;
 cout << "Enter (x y z):";
 cin >> x >> y >> z;
 cout << "Result is:" <<  1 + pow(min((x + y + z) / 2, x* y * z),2);
}


static void TaskTwo()
{
    long double x;
    cout << "Enter x:";
    cin >> x;

    if (x <= 0)
    {
        cout << "X is less or equal to 0, so result is:" << -x << endl;
    } else if (x <= 1) {cout << "X <= 1, but x > 0, so result is: " << x << endl;}
    else {cout << "X is bigger than 1, so result is: " << pow(x,2);}
}

static int TaskSelection() //Функция которая управляет выбором конкетного задания
 {
int numb;
cout << "Please, enter the number of task (or 0 for exit): ";
cin >> numb;

switch (numb)
{
case 0:
    return 0;
    break;
case 1:
    _Helper::GetArt(1);
    TaskOne();
    break;
case 2:
    _Helper::GetArt(2);
    TaskTwo();
    break;
default:
    cout << "Incorrect task number,pls try again;";
}
 }
}
